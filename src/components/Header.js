import React from 'react';
import styled from 'styled-components';
import Session from './../Session';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown, faUserEdit, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';

class Header extends React.Component {

    state = {
        showUserMenu : false
    }

    toogleUserMenu = () => { 
        console.log(!this.state.showUserMenu);
        this.setState({ showUserMenu : !this.state.showUserMenu })
    };

    render () {

        const HeaderStyle = styled.div`
            background-color: #FFFFFF;
            position : fixed;
            top: 0;
            left: 0;
            z-index: 10;
            width: 100%;
            padding-bottom : 10px;
            padding-top : 10px;

            img {
                float: left;
                height : 40px;
                margin-left : 5px;
            }

            p {
                text-align: left !important;
                font-size : 18px;
                font-weight : bold;
                display : inline-block;
                margin : 10px
            }
        `;

        const UserNav = styled.div`
            float : right;
            position: relative ;

            span, i {
                color : #263238;
                margin : 13px;
                display : inline-block;
                font-size : 14px;
            }

            img {
                border : 1px solid #CFD8DC;
                border-radius : 50%;
                height : 30px;
                float: left;
                margin-top: 5px;
            }


        `;

        const UserMenu = styled.div`
            position : absolute;
            right : 2%;
            background : #FFFFFF;
            border : 1px solid #CFD8DC;

            ul {
                list-style: none;
                margin : 0;
                padding : 0;
                width : 200px;

                li {                
                    padding : 10px;
                    color : #263238;
                    font-size : 14px;

                    :hover {
                        background-color : rgba( 0,0,0,0.2) ;
                        cursor : pointer;
                    }

                    svg {
                        margin-right : 10px;
                    }
                }
            }
        `;

        return (

            <HeaderStyle>
                <img src="./logo192.png" alt="" />
                <p className="AppName">MYSTATUS</p>

                <Session.Consumer>
                    {
                        ([userSession]) =>(
                            <UserNav>
                                <img src="https://alumni.crg.eu/sites/default/files/default_images/default-picture_0_0.png" alt=""/>
                                <span>{[userSession.name]}</span>
                                <span onClick={this.toogleUserMenu}>
                                    <FontAwesomeIcon icon={faAngleDown}/>
                                </span>
                                {
                                    this.state.showUserMenu ? 
                                    (
                                        <UserMenu>
                                            <ul>
                                                <li>
                                                    <FontAwesomeIcon icon={faUserEdit}/>
                                                    Update Account
                                                </li>
                                                <li>                            
                                                    <FontAwesomeIcon icon={faSignOutAlt}/>
                                                    Logout
                                                </li>
                                            </ul>
                                        </UserMenu>
                                    ) : null
                                }
                            </UserNav>
                        )                        
                    }
                </Session.Consumer>
            </HeaderStyle>
        )
    }

}

export default Header;
