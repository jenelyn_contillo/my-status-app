import React from 'react';
import styled from 'styled-components';
import firebase from './../firebase';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart, faComment } from '@fortawesome/free-solid-svg-icons';

class PostDetails extends React.Component {

    state = {
        liked : false,
    }

    handleIndexClick = event => {
        this.setState({
            liked: event.target.dataset.index
        });
      };

    render() {
        const PostData = styled.div`
        background : #FFFFFF;
        width : 600px;
        margin : 0 auto;
        padding : 20px;

        img {
            border : 1px solid #CFD8DC;
            border-radius : 50%;
            height : 30px;
            float: left;
            margin-top: 5px;
        }

        span{
            color : #263238;
            margin : 13px;
            display : inline-block;
            font-size : 14px;
        }

        .post {
            background-color : #F5F5F5;
            border : 1px solid #CFD8DC;
            padding : 2% ; 
            margin-top : 10px;
            outline : none;
            resize : none;
            width: 87%;
            margin-left: 2%;
        }
    `;

        return (
            <PostData>
                {
                    <div>
                        <img src="https://alumni.crg.eu/sites/default/files/default_images/default-picture_0_0.png" alt=""/>
                        <span className="userName">{this.props.name}</span>
                        <span className="dateTime">{this.props.date}</span>
                    </div>
                }
                <div>
                    <div className="post">
                        {this.props.post}
                    </div>
                    <div className="icons" >
                        <ul>
                            <li>
                                <FontAwesomeIcon icon={faHeart}/>
                                Like
                            </li>
                            <li>
                                <FontAwesomeIcon icon={faComment}/>
                                Comment
                            </li>
                        </ul>
                    </div>
                </div>
            </PostData>
    
        )
    }
}


export default PostDetails;