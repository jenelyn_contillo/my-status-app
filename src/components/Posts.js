import React, { useContext, useState } from 'react';
import Session from './../Session.js';
import { navigate } from '@reach/router';
import Header from  './Header';
import Footer from  './Footer';
import styled from 'styled-components';
import firebase from './../firebase';
import PostDetails from './PostDetails';

const Posts = () => {

    const [ userSession ] = useContext(Session);
    const [ post, setPost ] = useState("");
    const database = firebase.database();

    if( userSession === "" ) {
        navigate( '/' );
    }

    const addPost = () => {

        var postRef = database.ref('posts');
        var newPost = postRef.push();

        var date = new Date().getDate(); 
        var month = new Date().getMonth() + 1; 
        var year = new Date().getFullYear(); 
        var hours = new Date().getHours(); 
        var min = new Date().getMinutes(); 
        var sec = new Date().getSeconds(); 

        newPost.set({
            'user' : userSession.fid,
            'post' : post,
            'date' : year + '-' + month + '-' + date + ' ' + hours + ':' + min + ':' + sec
        });
    }

    const getPosts = async () => {

        var postRef = database.ref('posts');
        var userRef = database.ref("users");
        postRef.orderByChild("date").once("value")
            .then((snapshot) => {
                let posts = Object.entries(snapshot.val());

                posts.map( post => (  
                    userRef.child(post[1].user).once('value', user => {
                        let userInfo = user.val();
                        return (
                            <div>
                                {                            
                                    <PostDetails 
                                    post={post[1].post} 
                                    key={post[0]} 
                                    user={post[1].user}
                                    date={post[1].date}
                                    name={userInfo.name}
                                /> 
                                }
                            </div>
                        );
                        
                    })   
                                  
                )) 
                
                
        }); 
    }

    const PostContainer = styled.div`
        margin-bottom : 100px;
        margin-top : 100px;
    `;

    const PostForm = styled.div`
        background : #FFFFFF;
        width : 600px;
        margin : 0 auto;
        padding : 20px;

        img {
            border : 1px solid #CFD8DC;
            border-radius : 50%;
            height : 30px;
            float: left;
            margin-top: 5px;
        }

        textarea {
            background-color : #EEEEEE;
            border : none;
            padding : 2% ; 
            margin-top : 10px;
            outline : none;
            resize : none;
            width: 87%;
            margin-left: 2%;
        }

        .buttonHolder {

            button {
                float : right;
                margin-top: 10px;
                margin-right: 10px;
                width: 100px;
                padding: 5px 0px;
            }
        }

        .clear {
            clear: both
        }
    `;

    return (
        <div>
            <Header/>
            <PostContainer>
                <PostForm>
                    <form 
                        onSubmit = {(e) => {
                            e.preventDefault();
                            addPost();
                        }}
                    >
                        <img src="https://alumni.crg.eu/sites/default/files/default_images/default-picture_0_0.png" alt=""/>
                        <textarea 
                            id="post" 
                            rows="5" 
                            placeholder="What's on your mind?" 
                            onChange = {(e) => setPost(e.target.value)}
                            value = {post}
                        ></textarea>
                        <div className="buttonHolder">
                            <button className="button">Post</button>
                        </div>
                        <div className="clear"></div>
                    </form>
                </PostForm>
                {getPosts()}
            </PostContainer>
            <Footer/>
        </div>
    );
}

export default Posts;