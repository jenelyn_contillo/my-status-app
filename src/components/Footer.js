import React from 'react';
import styled from 'styled-components';

const Footer = () => {

    const FooterStyle = styled.div`
        background-color: #FFFFFF;
        position : fixed;
        bottom: 0;
        left: 0;
        z-index: 11;
        width: 100%;
        padding-bottom : 10px;
        padding-top : 10px;

        p {
            color : #09d3ac;
            text-align : center;
            margin : 10px;
        }
    `;


    return (

        <FooterStyle>
            <p>MYSTATUS APP &copy; 2019 </p>
        </FooterStyle>
    )

}

export default Footer;
