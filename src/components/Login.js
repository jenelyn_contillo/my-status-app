import React, { useContext } from 'react';
import styled from 'styled-components';
import { navigate } from '@reach/router';
import firebase from './../firebase';
import Session from './../Session.js';

const Login = () => {

    const database = firebase.database();

    const [ userSession, setUserSession ] = useContext(Session);

    if( userSession !== "" ) {
        navigate( '/posts' );
    }

    const Img = styled.img`
    display : block;
    height : 75px;
    margin : 0 auto;
    `;

    const SignDiv = styled.div`
        background-color: rgba(255, 255, 255, 1);
        margin : 100px auto;
        padding : 20px 10px 10px 10px;
        width: 400px;
    `;

    const SignInButton = styled.button`
        background-color : #BF360C;
        border : none;
        color : #FFFFFF;
        outline : none;
        padding : 2% 5% ; 
        width : 100%;
        margin-top : 10px;
    `;

    const FormInput = styled.input`
        background-color : #EEEEEE;
        border : none;
        padding : 2% 5% ; 
        width : 90%;
        margin-top : 10px;
        outline : none

    `;

    const FormSubmit = styled.button`
        padding : 2% 5% ; 
        width : 100%;
        margin-top : 10px;
    `;

    const Form = styled.form`
        margin : 20px 30px;
    `;

    const SignInWithGoogle = () => {
        let auth = firebase.auth();
        let provider = new firebase.auth.GoogleAuthProvider();
        auth.signInWithPopup(provider).then( (result) => {
            //var token = result.credential.accessToken;
            var user = result.user;
    
            if ( user.uid !== undefined ) {

                let usersRef = database.ref( "users" );

                usersRef.orderByChild("uid").equalTo( user.uid )
                    .once("value", snapshot => {

                        var  ID  = "";

                        if( snapshot.exists() ) {
                            
                            ID = Object.keys(snapshot.val())[0];

                        }else {
                            let newUser = usersRef.push();
                            newUser.set({
                                uid : user.uid,
                                name : user.displayName,
                                email : user.email
                            });
    
                            ID = newUser.key;
                        }

                        var data = {
                            uid : user.uid,
                            name : user.displayName,
                            email : user.email,
                            fid : ID
                        };
                
                        setUserSession( data );


                        navigate( '/posts' );

                    } ); 
            }
        });
    };

    return (
    <SignDiv>
        <Img src="./logo192.png" />
        <h3 className="AppName">MYSTATUS</h3>
        <Form>
            <FormInput placeholder="Username/Email Address" />
            <FormInput placeholder="Password" />
            <FormSubmit className="button">Sign in </FormSubmit>
            <SignInButton 
                type = "button"
                onClick = {SignInWithGoogle}
            >
                Sign in with Google
            </SignInButton>
        </Form>
    </SignDiv>
    )
}

export default Login;



