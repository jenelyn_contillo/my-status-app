import firebase from 'firebase';

firebase.initializeApp({
    apiKey: "AIzaSyB5xgDv9JZHJ6mznejC_QAO0PEXLHMiers",                             // Auth / General Use
    authDomain: "my-status-app-dad02.firebaseapp.com",         // Auth with popup/redirect
    databaseURL: "https://my-status-app-dad02.firebaseio.com/", // Realtime Database
    storageBucket: "my-status-app-dad02.appspot.com",          // Storage
    messagingSenderId: "949049359860"                  // Cloud Messaging
  });


  export default firebase;