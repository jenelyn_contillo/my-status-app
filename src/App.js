import React from 'react';
import './App.css';
import Login from './components/Login';
import { Router, navigate } from '@reach/router';
import Session from './Session';
import Posts from './components/Posts';
import firebase from 'firebase';


class App extends React.Component {

  state = {
    loading : true,
    userSession : []
  }

  componentDidMount() {

    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
  
        if ( user.uid !== undefined ) {
  
          var data = {
            uid : user.uid,
            name : user.displayName,
            email : user.email,
          };
    
          console.log(data);
    
          this.setState({ userSession : data, loading : false });    

          navigate('/posts/');
        }
      }
    });
  } 

  render() {
    if( this.state.loading ) {
      return <h1>Loading</h1>;
    }

    return (      
      <div className="App">
      <Session.Provider value={this.state.userSession}>
        <Router>
          <Login path="/" />
          <Posts path="/posts" />
        </Router>
      </Session.Provider>
    </div>

    );
  };
}

export default App;
